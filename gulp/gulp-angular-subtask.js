'use strict';

var gulp = require('gulp');
var browserSync = require('browser-sync');
var prompt = require('gulp-prompt');

var exec = require('child_process').exec;

var $ = require('gulp-load-plugins')();

module.exports = function (options) {
  gulp.task('gulp-angular-subtask', function () {

    console.log('Generate html, controller, css files for a new page')
    return gulp.src('*')
      .pipe(prompt.prompt({
        type: 'input',
        name: 'view',
        message: 'Name :?'
      }, function (res) {
        //value is in res.task (the name option gives the key)

        exec('yo gulp-angular-subtask:partial ' + res.view +' --html-type:html', function (err, stdout, stderr) {
          console.log(stdout);
          console.log(stderr);

          exec('yo gulp-angular-subtask:controller ' + res.view +' --script-type:js', function (err, stdout, stderr) {
            console.log(stdout);
            console.log(stderr);

            exec('yo gulp-angular-subtask:style ' + res.view + ' --style-type="scss"', function (err, stdout, stderr) {
              console.log(stdout);
              console.log(stderr);

              console.log('');
              console.log('TO ADD IN src/app/index.js');
              console.log('');
              console.log('.state(\''+res.view+'\', {');
              console.log(' url: \'/'+res.view+'\',');
              console.log(' templateUrl: \'app/'+res.view+'/partials/'+res.view+'.html\',');
              console.log(' controller: \''+res.view+'Ctrl\'');
              console.log('})');



              process.exit(0);
            });
          });
        });
      }));

  });
};
