'use strict';

angular.module('cfcFrontend', [
  'ngAnimate',
  'ngCookies',
  'ngTouch',
  'ngSanitize',
  'ngResource',
  'ui.router',
  'ui.bootstrap',
  'pascalprecht.translate',
  'angular-carousel'

])
  .config(function ($stateProvider, $urlRouterProvider ) {


    $stateProvider


      .state('admin', {
        abstract: true,
        url: '/admin',
        templateUrl: 'components/common/content.html'

      })
      .state('admin.carousel', {
        url: '/carousel',
        templateUrl: 'app/carousel/partials/carousel.html',
        controller: 'carouselCtrl'

      })

    $urlRouterProvider.otherwise('/admin/carousel');


  })
;


