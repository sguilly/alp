'use strict';
/**
 * @ngdoc function
 * @name cfcFrontend.controller:groupsCtrl
 * @description
 * # groupsCtrl
 * Controller of the cfcFrontend
 */
angular.module('cfcFrontend')
    .controller('carouselCtrl', function ($scope, $state, $stateParams) {

        console.log('load carouselCtrl');

        $scope.colors = ["#fc0003", "#f70008", "#f2000d", "#ed0012", "#e80017", "#e3001c", "#de0021", "#d90026", "#d4002b", "#cf0030", "#c90036", "#c4003b", "#bf0040", "#ba0045", "#b5004a", "#b0004f", "#ab0054", "#a60059", "#a1005e", "#9c0063", "#960069", "#91006e", "#8c0073", "#870078", "#82007d", "#7d0082", "#780087", "#73008c", "#6e0091", "#690096", "#63009c", "#5e00a1", "#5900a6", "#5400ab", "#4f00b0", "#4a00b5", "#4500ba", "#4000bf", "#3b00c4", "#3600c9", "#3000cf", "#2b00d4", "#2600d9", "#2100de", "#1c00e3", "#1700e8", "#1200ed", "#0d00f2", "#0800f7", "#0300fc"];


        $scope.array = {
            "g3": {
                "a1.jpg": [
                    null
                ],
                "a3.jpg": [
                    null
                ]
            },
            "gallery": {
                "10.jpg": [
                    null
                ],
                "10s.jpg": [
                    null
                ],
                "11.jpg": [
                    null
                ],
                "11s.jpg": [
                    null
                ],
                "12.jpg": [
                    null
                ],
                "12s.jpg": [
                    null
                ],
                "3.jpg": [
                    null
                ],
                "3s.jpg": [
                    null
                ],
                "4.jpg": [
                    null
                ],
                "4s.jpg": [
                    null
                ],
                "5.jpg": [
                    null
                ],
                "5s.jpg": [
                    null
                ],
                "6.jpg": [
                    null
                ],
                "6s.jpg": [
                    null
                ],
                "7.jpg": [
                    null
                ],
                "7s.jpg": [
                    null
                ],
                "8.jpg": [
                    null
                ],
                "8s.jpg": [
                    null
                ],
                "9.jpg": [
                    null
                ],
                "9s.jpg": [
                    null
                ],
                "ttt": [
                    "1.jpg",
                    "1s.jpg",
                    "2.jpg",
                    "2s.jpg"
                ]
            },
            "gallery2": {
                "a1.jpg": [
                    null
                ],
                "a2.jpg": [
                    null
                ],
                "a3.jpg": [
                    null
                ],
                "a4.jpg": [
                    null
                ],
                "a5.jpg": [
                    null
                ],
                "a6.jpg": [
                    null
                ],
                "a7.jpg": [
                    null
                ],
                "a8.jpg": [
                    null
                ],
                "dashbard4_1.jpg": [
                    null
                ],
                "dashbard4_2.jpg": [
                    null
                ],
                "email_1.jpg": [
                    null
                ],
                "email_2.jpg": [
                    null
                ],
                "email_3.jpg": [
                    null
                ],
                "p1.jpg": [
                    null
                ],
                "p2.jpg": [
                    null
                ],
                "p3.jpg": [
                    null
                ],
                "p4.jpg": [
                    null
                ],
                "p5.jpg": [
                    null
                ],
                "p6.jpg": [
                    null
                ]
            }
        };

        function addSlide(target, style) {
            var i = target.length;
            target.push({
                id: (i + 1),
                label: 'slide !' + (i + 1),
                img: 'http://lorempixel.com/450/300/' + style + '/' + ((i + 1) % 10),
                color: $scope.colors[(i * 10) % $scope.colors.length],
                odd: (i % 2 === 0)
            });
        };

        $scope.carouselIndex = 3;

        function addSlides(target, style, qty) {
            for (var i = 0; i < qty; i++) {
                addSlide(target, style);
            }
        }

        // 1st ngRepeat demo
        $scope.slides = [];
        addSlides($scope.slides, 'sports', 50);


    });
