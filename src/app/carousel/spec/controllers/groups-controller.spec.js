'use strict';
describe('Controller: groupsCtrl', function () {
// load the controller's module
  beforeEach(module('cfcFrontend'));
  var groupsCtrl,
      scope;
// Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    groupsCtrl = $controller('groupsCtrl', {
      $scope: scope
    });
  }));
  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
